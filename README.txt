Data and Scripts for the article "Comparison of enrichment methods for efficient nitrogen fixation on a biocathode" by Axel Rous, Gaëlle Santa-Catalina, Elie Desmond-Le Quémener, Eric Trably and Nicolas Bernet

R scripts used for data analysis are all presents in script folder
All data are presents in data folder

Read the README in script folder to know the order to run each script