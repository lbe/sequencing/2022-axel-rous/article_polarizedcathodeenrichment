Scripts for analysing raw data stored in data folder
Running order :

1-qPCR.R (import data from qPCR as resumed in Samples.xslx file, raw data available in raw data folder, allow production of plot for growth as presented in article "plotcroiss")
2-Sequence.R (import mapping data from Samples.xlsx, and sequencing raw data resume in exp02-03-04-resume003.xlsx file (biom and raw fasta are store in rawqPCR folder), plot Community as presented in article at the end of script)
3-ARA.R (import and treatment of ARA results from gaz, also gaz composition for batch with ARA to add to H3 object from gaz.R script)
4-gaz.R (import Date, gaz composition + pressure, Volume and DO from H3C1.xlsx, Enrichissement.xslx and H3Nfree7.Xlsx, need H3C2B10 object from ARA.R script)
5-Gallery.R (raw data from galley analysis and treatment)
6-Nper16S.R (transformation of bacteria count to N mass, needed for CHNSResults.R)
7-CHNSresults.R (import CHNS raw results and resume from other measurement as N-NH4 from Gallery.R (N-NH4 from batch end),also plot C/N, total N and N-NH4 as presented in article)
8-Script_CA.Rmd (import Chronoamperometry data, create individual data frame for each batch and compil in one file, when completed with H3 object and Gallery object, plotCA to plot the chronoamperometry data as in article)
9-Compilation_R (calcul line used for resume data table in article)
10-calculs_bilan.R (calcul for validation of CE calcul from CHNSresulst.R for one batch and calcul from community analysis of heterotrophic/acetogenic)


All dependencies (library) are indicated at beginning of script when needed