#Code R pour les bilans d'élecrons sur les manips d'Axel
#Elie Le Quéméner, Axel Rous
#Creation: 13/12/2022
#Modification: 27/02/2023

################################################
#######   Calcul du bilan d'e- sous R   ########
################################################


#Data from first PCE to validate CHNSResults script calcul
NNH4 = 0.01 #mg N/l
NNH42 = 0.2
NNO3 = 0.02 #mg N/l
NBio = 3 #mg/l
NBio2 = 9.6
acet = 0.728 #g/l
acet2 = 0.37
nH2 = 8.6 #mmol
nH22 = 0.11
ne = 364.2 #mmol e-
ne2 = 149.6
Bio_bulk = 53025620 #bactérie/l
Bio_bulk2 = 51564101000
Bio_cath = 9507571980 #bactérie/l/j
Bio_cath2 = 45756830960


duree = 25 #j
#m_Bio_bulk = 37 #mg/l

#Masse de biomasse sur les électrodes :
#nc16S = 4.1 #Nombre de copie de 16S en moyenne par bact d'après les chiffres d'Axel
mBact = 2.16e-13 #g/bact d'après le doc de Roman/Javiera
m_Bio_cath = Bio_cath*duree*mBact*1000 #mg/l
m_Bio_cath2 = Bio_cath2*duree*mBact*1000
m_Bio_bulk = Bio_bulk*mBact*1000
m_Bio_bulk2 = Bio_bulk2*mBact*1000

#[1] 0.03567073 g/l
#Assez différent du calcul d'Axel qui donne plutôt 0.166 mg/l. Probablement dû à une différence dans mBact.
#Je reprends le calcul d'Axel :
#m_Bio_cath = 6.65*duree #mg/l d'après Axel

#Constantes :
MC = 12 #g/mol
MH = 1 #g/mol
MO = 16 #g/mol
MN = 14 #g/mol
Mbio = 1*MC + 1.8*MH + 0.5*MO + 0.2*MN #g/mol car formule brute de la biomasse CH1.8O0.5N0.2
Mace = 2*MC + 3*MH + 2*MO #g/mol

theo_N = 0.2*MN/Mbio
m_N_theo_cath = m_Bio_cath*theo_N #mgN/l
m_N_theo_Bulk = m_Bio_bulk*theo_N
m_N_theo_cath2 = m_Bio_cath2*theo_N #mgN/l
m_N_theo_Bulk2 = m_Bio_bulk2*theo_N

#Conversion de moles en moles d'électrons -> package CHNOSZ : ça revient à écrire les demi-équations électroniques pour chaque molécule
library(CHNOSZ)
add.OBIGT("new_data_CHNOSZ_test.csv") #On ajoute la biomasse CH1.8O0.5N0.2 dans CHNOSZ
basis(c("CO2", "NH4+", "H+", "H2O","e-")) #On fait les bilans d'électrons à partir des molécules de base listées ici
gamma_H2 = subcrt("H2", 1)$reaction["2","coeff"] #mol e-/mol H2
gamma_NH4 = subcrt(c("NH4+","N2"),c(1,-0.5))$reaction["2","coeff"] #mol e-/mol NH4+
gamma_NO3 = subcrt("NO3-", 1)$reaction["2","coeff"] #mol e-/mol NO3-
gamma_bio = subcrt("biomass", 1)$reaction["2","coeff"] #mol e-/mol biomass
gamma_ace = subcrt("acetate", 1)$reaction["2","coeff"] #mol e-/mol acetate

#Bilan
#On convertit tout en mmol
nNH4 = NNH4/MN #mmol/l
nNO3 = NNO3/MN #mmol/l
nNBio = NBio/MN #mmol/l
nNtheoBulk = m_N_theo_Bulk/MN
nNtheocath = m_N_theo_cath/MN
nacet = acet/Mace*1000 #mmol/l
nBio_cath = m_Bio_cath/Mbio #mmol/l
nBio_bulk = m_Bio_bulk/Mbio #mmol/l

nNH42 = NNH42/MN #mmol/l
nNBio2 = NBio2/MN #mmol/l
nNtheoBulk2 = m_N_theo_Bulk2/MN
nNtheocath2 = m_N_theo_cath2/MN
nacet2 = acet2/Mace*1000 #mmol/l
nBio_cath2 = m_Bio_cath2/Mbio #mmol/l
nBio_bulk2 = m_Bio_bulk2/Mbio #mmol/l


#En mmol e-
ne_H2 = nH2*gamma_H2
ne_NH4 = nNH4*gamma_NH4*0.85
ne_NO3 = nNO3*gamma_NO3*0.85
ne_NBio = nNBio*gamma_NH4*0.85
ne_ace = nacet*gamma_ace*0.85
ne_Bio_cath = nBio_cath*gamma_bio*0.85
ne_Bio_bulk = nBio_bulk*gamma_bio*0.85
ne_Bio_N_Bulk = nNtheoBulk*gamma_NH4*0.85
ne_Bio_N_cath = nNtheocath*gamma_NH4*0.85

ne_H22 = nH22*gamma_H2
ne_NH42 = nNH42*gamma_NH4*0.85
ne_NBio2 = nNBio2*gamma_NH4*0.85
ne_ace2 = nacet2*gamma_ace*0.85
ne_Bio_cath2 = nBio_cath2*gamma_bio*0.85
ne_Bio_bulk2 = nBio_bulk2*gamma_bio*0.85
ne_Bio_N_Bulk2 = nNtheoBulk2*gamma_NH4*0.85
ne_Bio_N_cath2 = nNtheocath2*gamma_NH4*0.85

 
            # H2          NH4+         NO3-   acetate cathodic biomass bulk biomass      total
# mmol e- -17.20000 -0.0021428571  0.007142857 -98.71186       -32.439024    -7.219512 -155.56540
# %         4.72268  0.0005883737 -0.001961246  27.10375         8.906926     1.982293   42.71428

# H2          NH4+         NO3-     N Bulk    N Bulk bio     N cath   acetate cathodic biomass  bulk biomass     total
# mmol e- -17.20000 -0.0018214286  0.009714286 -0.4542188 -1.014103e-07 -0.5464286 -83.90508        -5.169970 -1.154264e-06 -107.2678
# %         4.72268  0.0005001177 -0.002667294  0.1247168  2.784468e-08  0.1500353  23.03819         1.419541  3.169313e-07   29.4530
##Calcul de biomasse différent pour N, (11.3% de MBio ou 13% de mBio chez moi correspondant à 1.6%)




##Calcul part acétogène et hétérotrophe

# mBact_total = mBact_mesuré = mBact_acet+mBact_heter
# mAcetate = mAcetate_mesuré = mAcetate_produit-mAcetate_consommé
# 
# 0.5H2+0.25CO2+0.002NH3->0.01CH1.4O0.4N0.2+0.121CH3COOH    https://www.sciencedirect.com/science/article/pii/S254243512030177X
# 0.121CH3COOH+0.087O2+0.031NH3->0.155CH1.4O0.4N+0.087CO2+0.18H2O
# 
# MAcetate=60 g/mol
# Mbiomasse=22.6 g/mol
# 
# Soit 0.01Biomasse_acetogene et 0.121CH3COOH produit par acetogene et 0.121CH3COOH pour 0.155Biomasse ou
# 0.226 g Biomasse_acetogene produisent 7.26 g Acetate et 7.26g acetate consommé pour 3.503 g Biomasse_hetero
# 
# D ou mAcetate_produit= 32.12*mBact_acet
# et mAcetate_consommé= 2.07*mBact_heter
# 
# Donc mAcetate = 32.12*mBact_acet-2.07*mbact_heter
# mAcetate = 32.12*mBact_acet-2.07*(mBact_mesuré-mBact_acet)
# mAcetate = 34.19*mBact_acet-2.07*mBact_mesuré
# 
# mBact_acet = (mAcetate+2.07*mBact_mesuré)/34.19
# 
# mBact_heter = mBact_mesuré - mBact_acet
# mBact_heter =(32.12*mBact_mesuré-mAcetate)/34.19

Bio_cath_aceto = (acet+2.07*Bio_cath*duree*mBact)/34.19*1000 #mg/L
Bio_cath_heter = (32.12*Bio_cath*duree*mBact-acet)/34.19*1000

part_hete = Bio_cath_heter/(Bio_cath*duree*mBact*1000)*100 #%
part_hete

mAcetate_tot = acet*1000+2.07*Bio_cath_heter  #mg/L

nBio_cath_aceto=Bio_cath_aceto/Mbio  #mmol/L
nBio_cath_heter=Bio_cath_heter/Mbio
nacet_total=mAcetate_tot/Mace

neBio_cath_aceto=nBio_cath_aceto*gamma_bio*0.85   #mmol e
neBio_cath_heter=nBio_cath_heter*gamma_bio*0.85
neacet_total=nacet_total*gamma_ace*0.85



Bio_cath_aceto2 = (acet2+2.07*Bio_cath2*duree*mBact)/34.19*1000
Bio_cath_heter2 = (32.12*Bio_cath2*duree*mBact-acet2)/34.19*1000

part_hete2 = Bio_cath_heter2/(Bio_cath2*duree*mBact*1000)*100
part_hete2

mAcetate_tot2 = acet2*1000+2.07*Bio_cath_heter2

nBio_cath_aceto2=Bio_cath_aceto2/Mbio
nBio_cath_heter2=Bio_cath_heter2/Mbio
nacet_total2=mAcetate_tot2/Mace

neBio_cath_aceto2=nBio_cath_aceto2*gamma_bio*0.85
neBio_cath_heter2=nBio_cath_heter2*gamma_bio*0.85
neacet_total2=nacet_total2*gamma_ace*0.85


#Bilan mmol e-
mat_ne <- matrix(0,2,11) #on créée la matrice qui résume les bilans
colnames(mat_ne) <- c("H2","N fixed total","acetate","cathodic biomass","bulk biomass","total","Biomasse acetogene","acetate total","total theorique","heterotrophe","acetogene")
rownames(mat_ne) <- c("mmol e-","%")
mat_ne[1,c(1:5,7:8)] <- c(ne_H2, ne_NH4+ ne_NO3+ ne_NBio+ ne_Bio_N_Bulk+ ne_Bio_N_cath, ne_ace, ne_Bio_cath, ne_Bio_bulk, neBio_cath_aceto, neacet_total)
mat_ne[1,6] = sum(mat_ne[1,c(1:5)])
mat_ne[1,9] = sum(mat_ne[1,c(1:2,7:8)])
mat_ne[2,1:9] <- -mat_ne[1,1:9]/ne*100
mat_ne[2,10:11] <- c(part_hete,100-part_hete)

mat_ne2 <- matrix(0,2,11) #on créée la matrice qui résume les bilans
colnames(mat_ne2) <- c("H2","N fixed total","acetate","cathodic biomass","bulk biomass","total","Biomasse acetogene","acetate total","total theorique","heterotrophe","acetogene")
rownames(mat_ne2) <- c("mmol e-","%")
mat_ne2[1,c(1:5,7:8)] <- c(ne_H22, ne_NH42 + ne_NBio2 + ne_Bio_N_Bulk2 + ne_Bio_N_cath2, ne_ace2, ne_Bio_cath2, ne_Bio_bulk2, neBio_cath_aceto2, neacet_total2)
mat_ne2[1,6] = sum(mat_ne2[1,c(1:5)])
mat_ne2[1,9] = sum(mat_ne2[1,c(1:2,7:8)])
mat_ne2[2,1:9] <- -mat_ne2[1,1:9]/ne2*100
mat_ne2[2,10:11] <- c(part_hete2,100-part_hete2)

mat_ne  ## total = ancien total avec biomasse correspondant à biomasse mesurée totale et total théorique = nouveau avec Biomasse acetogene et acetate total en remplacement de cathodic biomass et acetate
mat_ne2 


library(tidyverse)

mixoW1<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W1"))&str_detect(pdata2$Family,"Rhodospirillaceae|Lachnospiraceae")&pdata2$cycle=="Reacteur pauvre 100j"])
mixoW3<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W3"))&str_detect(pdata2$Family,"Rhodospirillaceae|Lachnospiraceae")&pdata2$cycle=="Reacteur pauvre 100j"])


autotrW1<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W1"))&pdata2$Family%in%c("Peptococcaceae","Xanthobacteraceae","Clostridiaceae_1","Clostridiales_Incertae_Sedis","Gracilibacteraceae","Lachnospiraceae","Hydrogenophilaceae")&pdata2$cycle=="Reacteur pauvre 100j"])
autotrW3<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W3"))&pdata2$Family%in%c("Peptococcaceae","Xanthobacteraceae","Clostridiaceae_1","Clostridiales_Incertae_Sedis","Gracilibacteraceae","Lachnospiraceae","Hydrogenophilaceae")&pdata2$cycle=="Reacteur pauvre 100j"])


heteroW1<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W1"))&pdata2$Family%in%c("Comamonadaceae","Xanthomonadaceae","Enterococcaceae","Enterobacteriaceae","Bacillaceae","Alcaligenaceae","Caulobacteraceae","Rhodocyclaceae","Pseudomonadaceae","Paenibacillaceae","Coriobacteriaceae","Porphyromonadaceae","Rhodospirillaceae")&pdata2$cycle=="Reacteur pauvre 100j"])
heteroW3<-sum(pdata2$bact[(str_detect(pdata2$Sample,"W3"))&pdata2$Family%in%c("Comamonadaceae","Xanthomonadaceae","Enterococcaceae","Enterobacteriaceae","Bacillaceae","Alcaligenaceae","Caulobacteraceae","Rhodocyclaceae","Pseudomonadaceae","Paenibacillaceae","Coriobacteriaceae","Porphyromonadaceae","Rhodospirillaceae")&pdata2$cycle=="Reacteur pauvre 100j"])

heteroW1/sum(autotrW1,heteroW1)
heteroW3/sum(autotrW3,heteroW3)


