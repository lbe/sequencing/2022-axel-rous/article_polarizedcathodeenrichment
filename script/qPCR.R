library(readxl)
library(ggplot2)
library(ggpattern)
library(cowplot)
library(tidyverse)
library(ggpubr)
library(lubridate)
library(car)
library(ggallin)
library(ggbreak)

wd<-getwd()

qPCR <- read_excel(paste(wd,"/data/qPCR/Samples.xlsx",sep=""),range = "A1:Y78") |> 
  select(Sample,`16s`,`sd 16s`, nifH,`sd nifH`,DO16s,DO_ARA,cycle,electrode,`MS milieu`,names,VarDO,temps_abs,MEC,Duree)|> 
  filter(names!="Rea Mix pol 5_0a"&names!="Leg a"&names!="Rea Mix pol 6a"&names!="Rea Mix OCV 6a"&names!="Elec Sep pol 6a"&names!="Elec Sep OCV 6b"&names!="Elec Mix pol 6a"&names!="Elec Mix OCV 6a") |> 
  mutate(`16s`=case_when(electrode!="Electrode"~as.numeric(`16s`),
                         electrode=="Electrode"&str_detect(cycle,"15j")~as.numeric(`16s`)*20*25/850,
                         electrode=="Electrode"&str_detect(cycle,"100j")~as.numeric(`16s`)*20*24/850,
                         electrode=="Electrode"&str_detect(cycle,"120j")~as.numeric(`16s`)*20*23/850,
                         electrode=="Electrode"&str_detect(cycle,"200j")~as.numeric(`16s`)*20*22/850)) |> 
  mutate(nifH=case_when(electrode!="Electrode"~as.numeric(nifH),
                        electrode=="Electrode"&str_detect(cycle,"15j")~as.numeric(nifH)*20*25/850,
                        electrode=="Electrode"&str_detect(cycle,"100j")~as.numeric(nifH)*20*24/850,
                        electrode=="Electrode"&str_detect(cycle,"120j")~as.numeric(nifH)*20*23/850,
                        electrode=="Electrode"&str_detect(cycle,"200j")~as.numeric(nifH)*20*22/850)) |> 
  mutate(`sd 16s`=case_when(electrode!="Electrode"~as.numeric(`sd 16s`),
                            electrode=="Electrode"&str_detect(cycle,"15j")~as.numeric(`sd 16s`)*20*25/850,
                            electrode=="Electrode"&str_detect(cycle,"100j")~as.numeric(`sd 16s`)*20*24/850,
                            electrode=="Electrode"&str_detect(cycle,"120j")~as.numeric(`sd 16s`)*20*23/850,
                            electrode=="Electrode"&str_detect(cycle,"200j")~as.numeric(`sd 16s`)*20*22/850)) |> 
  mutate(`sd nifH`=case_when(electrode!="Electrode"~as.numeric(`sd nifH`),
                             electrode=="Electrode"&str_detect(cycle,"15j")~as.numeric(`sd nifH`)*20*25/850,
                             electrode=="Electrode"&str_detect(cycle,"100j")~as.numeric(`sd nifH`)*20*24/850,
                             electrode=="Electrode"&str_detect(cycle,"120j")~as.numeric(`sd nifH`)*20*23/850,
                             electrode=="Electrode"&str_detect(cycle,"200j")~as.numeric(`sd nifH`)*20*22/850)) |> 
  mutate(MW=case_when(!str_detect(cycle,"Reacteur")~as.numeric(`MS milieu`)*0.045,
                      str_detect(cycle,"Reacteur")~as.numeric(`MS milieu`)*0.85)) |> 
  mutate(nifHX16=as.numeric(nifH)/as.numeric(`16s`)) |>
  mutate(bact=NA) |> 
  mutate(sdbact=NA)


tab<-read_xlsx(paste(wd,"/data/qPCR/exp02-03-04-resume003.xlsx",sep=""),sheet="final.pick.Bacteria.an.0.03 (2") |> 
  select(-`V3_GG_09_H3C2B5t0_R3`,-`V3_GG_12_H3_L1`,-`V3_GG_17_H3C2B6t20R3`,-`V3_GG_18_H3C2B6t20R4`,-`V3_GG_19_H3C2B6t20W1`,-`V3_GG_20b_H3C2B6t20W2`,-`V3_GG_21_H3C2B6t20W3`,-`V3_GG_22_H3C2B6t20W4`) |> 
  rename(Kingdom="Domain")  |> 
  filter(!is.na(OTU))

mappage <- read_xlsx(paste(wd,"/data/qPCR/Samples.xlsx",sep=""),range="A1:AA78",na="NA") |>
  mutate(SampleID=Sample) |>
  column_to_rownames("Sample") |>
  mutate(Group = cycle) |>
  mutate(cycle.medium=paste(cycle,medium,sep=".")) |> 
  mutate(contenant.medium=paste(contenant,medium,sep=".")) |> 
  mutate(electrode.potential=paste(electrode,potential,sep=".")) |> 
  filter(names!="Rea Mix pol 5_0a"&names!="Leg a"&names!="Rea Mix pol 6a"&names!="Rea Mix OCV 6a"&names!="Elec Sep pol 6a"&names!="Elec Sep OCV 6b"&names!="Elec Mix pol 6a"&names!="Elec Mix OCV 6a")


data16s<-filter(select(read_delim(paste(wd,"/data/qPCR/rrnDB-5.7_pantaxa_stats_NCBI.txt",sep="")),rank,name,mean,stddev),rank!="species")  ##bdd nombre de bactérie théorique par rapport aux nombre de 16s par species/genus/family/class

mappage <- mappage |> 
  mutate(cycle=case_when(str_detect(cycle,"Pré")~paste("Bouteille",str_extract(cycle,"riche|pauvre"),"0j",sep=" "),
                         !str_detect(cycle,"Pré")~cycle)) |> 
  arrange(desc(contenant),desc(medium),as.numeric(gsub("[^[:digit:]]","",cycle)),potential)

rownames(mappage)<-gsub("V3_","",rownames(mappage)) 


taxmat = tab[,c(1,2,4,6,8,10,12)] |>   #On récupère la taxonomie
  mutate(Kingdom=paste("k_",Kingdom,sep="_")) |> 
  mutate(Phylum=paste("p_",Phylum,sep="_")) |>
  mutate(Class=paste("c_",Class,sep="_")) |> 
  mutate(Order=paste("o_",Order,sep="_")) |> 
  mutate(Family=paste("f_",Family,sep="_")) |> 
  mutate(Genus=paste("g_",Genus,sep="_")) |> 
  mutate(Species="s__") |> 
  column_to_rownames("OTU")|>
  mutate(copies=NA) |>
  mutate(stddev=NA)

for (i in 1:length(taxmat$Genus)){   ##ajout 16s/bacterie en fonction genre/famille/moyenne si non connu pour chaque otu de notre table tax
  for(j in 1:length(data16s$name)){
    if(gsub("g__","",taxmat$Genus[i])==data16s$name[j]){
      taxmat$copies[i]=data16s$mean[j]
      taxmat$stddev[i]=data16s$stddev[j]
    }
    if(is.na(taxmat$copies[i])){
      if(length(data16s$mean[data16s$name==gsub("_1","",gsub("f__","",taxmat$Family[i]))])!=0){
        taxmat$copies[i]=data16s$mean[data16s$name==gsub("_1","",gsub("f__","",taxmat$Family[i]))]
        taxmat$stddev[i]=data16s$stddev[data16s$name==gsub("_1","",gsub("f__","",taxmat$Family[i]))]
      }
      if(length(data16s$mean[data16s$name==gsub("_1","",gsub("f__","",taxmat$Family[i]))])==0){
        taxmat$copies[i]=mean(data16s$mean)
        taxmat$stddev[i]=mean(data16s$stddev)
      }
    }
  }
}


otumat = tab |>  #On récupère les abondances relatives
  column_to_rownames("OTU") |> 
  select(14:length(tab)-1)

otumatmean16s=otumat

pre_map<-mappage |>
  mutate(`16s`=case_when(electrode=="Electrode"&str_detect(cycle,"15j")~`16s`*20*25/850,
                         electrode=="Electrode"&str_detect(cycle,"100j")~`16s`*20*24/850,
                         electrode=="Electrode"&str_detect(cycle,"120j")~`16s`*20*23/850,
                         electrode=="Electrode"&str_detect(cycle,"200j")~`16s`*20*22/850,
                         contenant=="B"~`16s`,
                         electrode=="Bulk"&contenant=="A"~`16s`)) |>
  mutate(nifH=case_when(electrode=="Electrode"&str_detect(cycle,"15j")~nifH*20*25/850,
                        electrode=="Electrode"&str_detect(cycle,"100j")~nifH*20*24/850,
                        electrode=="Electrode"&str_detect(cycle,"120j")~nifH*20*23/850,
                        electrode=="Electrode"&str_detect(cycle,"200j")~nifH*20*22/850,
                        contenant=="B"~nifH,
                        electrode=="Bulk"&contenant=="A"~nifH)) |> 
  mutate(`sd 16s`=case_when(electrode=="Electrode"&str_detect(cycle,"15j")~`sd 16s`*20*25/850,
                            electrode=="Electrode"&str_detect(cycle,"100j")~`sd 16s`*20*24/850,
                            electrode=="Electrode"&str_detect(cycle,"120j")~`sd 16s`*20*23/850,
                            electrode=="Electrode"&str_detect(cycle,"200j")~`sd 16s`*20*22/850,
                            contenant=="B"~`sd 16s`,
                            electrode=="Bulk"&contenant=="A"~`sd 16s`)) |>
  mutate(`sd nifH`=case_when(electrode=="Electrode"&str_detect(cycle,"15j")~`sd nifH`*20*25/850,
                             electrode=="Electrode"&str_detect(cycle,"100j")~`sd nifH`*20*24/850,
                             electrode=="Electrode"&str_detect(cycle,"120j")~`sd nifH`*20*23/850,
                             electrode=="Electrode"&str_detect(cycle,"200j")~`sd nifH`*20*22/850,
                             contenant=="B"~`sd nifH`,
                             electrode=="Bulk"&contenant=="A"~`sd nifH`))


for(i in 1:length(rownames(otumatmean16s)))  {
  for(j in 1:length(rownames(taxmat))){
    if (rownames(otumatmean16s)[i]==rownames(taxmat)[j]){
      for (k in 1:length(otumatmean16s)){
        otumatmean16s[i,k]=(otumat[i,k]/sum(otumat[,k])*pre_map$`16s`[rownames(pre_map)==gsub("V3_","",gsub("GG_","",gsub("AR_","",names(otumatmean16s)[k])))]/(taxmat$copies[j]))#)##calcul nombre de bactérie théorique par rapport aux nombre de 16s pour chaque otu (couplage qPCR16s et 16s/bact de taxtable) avec décalage avec stddev
      }
    }
  }
}

otutotmean16s<-otumatmean16s[1,]
otutotmean16s[1,]<-NA

for(i in 1:length(otumatmean16s)){
  otutotmean16s[1,i]<-sum(otumatmean16s[,i])
  for(k in 1:length(rownames(pre_map))){
    for(j in 1:length(rownames(otumatmean16s))){ 
      if(rownames(pre_map)[k]==gsub("V3_","",gsub("GG_","",gsub("AR_","",names(otumatmean16s[i]))))){
        otumatmean16s[j,i]<-(otumatmean16s[j,i]/otutotmean16s[1,i])
      }
    }
  }
}

map<-pre_map  |> 
  mutate(Time=as.integer(gsub("[^[:digit:]]","",cycle))) |> 
  mutate(Rep=1) |>
  select(SampleID,nifH,medium,potential,electrode,enrichment,contenant,cycle,names,Group,cycle.medium,contenant.medium,electrode.potential,Time,Rep)

for(i in 1:length(rownames(map))){
  for(j in 1:length(otutotmean16s)){
    if(rownames(map)[i]==gsub("V3_","",gsub("GG_","",gsub("AR_","",names(otutotmean16s)[j])))){
      map$`16s`[i]=otutotmean16s[j]
    }
  }
}

for(i in 1:length(map$names)){     
  for(j in 1:length(qPCR$Sample)){
    if(gsub("Elec ","",gsub("Rea ","",map$names[i]))==gsub("Elec ","",gsub("Rea ","",gsub("V3_","",qPCR$names[j])))&qPCR$electrode[j]==map$electrode[i]){#gsub("ulk","",(gsub("-2021","_",qPCR$nom[j]))))
      qPCR$bact[j]=as.numeric(map$`16s`[i])
    }
  }
}

qPCR<-qPCR|> 
  # filter(!str_detect(cycle,"Pré")) |> #&!is.na(bact)) |> 
  mutate(Sample=gsub("WE","",gsub("Bulk","",gsub("Rea ","",gsub("Elec ","",names))))) |> 
  rename(X16s=`16s`) |> 
  mutate(Var16S=NA) |> 
  select(-names)

for(i in 1:length(qPCR$Sample)){
  if(qPCR$electrode[i]=="Electrode"){
    qPCR<-qPCR |> 
      add_row(select(qPCR[i,],-electrode),electrode="Total")
  }
}

for(i in 1:length(qPCR$Sample)){
  if(qPCR$electrode[i]=="Total"&length(qPCR$X16s[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"])!=0){
    qPCR$X16s[i]=qPCR$X16s[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$X16s[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"] 
    qPCR$nifH[i]=qPCR$nifH[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$nifH[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"] 
    qPCR$bact[i]=qPCR$bact[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$bact[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
    qPCR$`sd 16s`[i]=qPCR$`sd 16s`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$`sd 16s`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"] 
    qPCR$`sd nifH`[i]=qPCR$`sd nifH`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$`sd nifH`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"] 
    qPCR$sdbact[i]=qPCR$sdbact[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$sdbact[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
    qPCR$nifHX16[i]=qPCR$nifHX16[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$nifHX16[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
    qPCR$DO_ARA[i]=qPCR$DO_ARA[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$DO_ARA[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
    qPCR$MW[i]=qPCR$MW[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$MW[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
    qPCR$`MS milieu`[i]=qPCR$`MS milieu`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Electrode"]+qPCR$`MS milieu`[qPCR$Sample==qPCR$Sample[i]&qPCR$electrode=="Bulk"]
  }
}


qPCRfin<-filter(mutate(mutate(mutate(mutate(mutate(filter(qPCR,(cycle%in%c("Pré-enrichissement riche","Pré-enrichissement pauvre","Bouteille pauvre 45j","Bouteille pauvre 200j","Bouteille pauvre 300j","Bouteille riche 15j","Reacteur pauvre 15j","Reacteur pauvre 15j OCV","Reacteur pauvre 100j","Reacteur pauvre 100j OCV","Reacteur pauvre 120j","Reacteur pauvre 120j OCV","Reacteur pauvre 200j","Reacteur pauvre 200j OCV")&electrode!="Bulk")|str_detect(cycle,"Reacteur riche")),cycle=case_when(cycle=="Pré-enrichissement riche"~"Organic C Bottle 00d",cycle=="Pré-enrichissement pauvre"~"H2 Bottle 00d",cycle=="Bouteille pauvre 45j"~"H2 Bottle 45d",cycle=="Bouteille pauvre 200j"~"H2 Bottle 200d",cycle=="Bouteille pauvre 300j"~"H2 Bottle 300d",cycle=="Bouteille riche 15j"~"Organic C Bottle 15d",cycle=="Reacteur riche 45j"~"MEC org 100d",cycle=="Reacteur riche 45j OCV"~"OCV org 100d",cycle=="Reacteur pauvre 15j"~"MEC 125d",cycle=="Reacteur pauvre 15j OCV"~"OCV 125d",cycle=="Reacteur pauvre 100j"~"MEC 210d",cycle=="Reacteur pauvre 100j OCV"~"OCV 210d",cycle=="Reacteur pauvre 120j"~"MEC 230d",cycle=="Reacteur pauvre 120j OCV"~"OCV 230d",cycle=="Reacteur pauvre 200j"~"MEC 300d",cycle=="Reacteur pauvre 200j OCV"~"OCV 300d")),cycle=factor(cycle,levels=c("Organic C Bottle 00d","H2 Bottle 00d","H2 Bottle 45d","Organic C Bottle 15d","H2 Bottle 200d","H2 Bottle 300d","MEC org 100d","OCV org 100d","MEC 125d","OCV 125d","MEC 210d","OCV 210d","MEC 230d","OCV 230d","MEC 300d","OCV 300d"))),type=case_when(str_detect(cycle,"H2")~"H2",!str_detect(cycle,"H2")~str_sub(cycle,1,3))),enrich=case_when(type%in%c("H2","MEC","OCV")~type,type=="Org"~"MEC")),X16s=case_when(str_detect(cycle,"org 100d")~NaN,!str_detect(cycle,"org 100d")~X16s),bact=case_when(str_detect(cycle,"org 100d")~NaN,!str_detect(cycle,"org 100d")~bact))) |> 
  add_row(enrich="OCV",filter(mutate(mutate(mutate(mutate(mutate(filter(qPCR,(cycle%in%c("Pré-enrichissement riche","Pré-enrichissement pauvre","Bouteille pauvre 45j","Bouteille pauvre 200j","Bouteille pauvre 300j","Bouteille riche 15j","Reacteur pauvre 15j","Reacteur pauvre 15j OCV","Reacteur pauvre 100j","Reacteur pauvre 100j OCV","Reacteur pauvre 120j","Reacteur pauvre 120j OCV","Reacteur pauvre 200j","Reacteur pauvre 200j OCV")&electrode!="Bulk")|str_detect(cycle,"Reacteur riche")),cycle=case_when(cycle=="Pré-enrichissement riche"~"Organic C Bottle 00d",cycle=="Pré-enrichissement pauvre"~"H2 Bottle 00d",cycle=="Bouteille pauvre 45j"~"H2 Bottle 45d",cycle=="Bouteille pauvre 200j"~"H2 Bottle 200d",cycle=="Bouteille pauvre 300j"~"H2 Bottle 300d",cycle=="Bouteille riche 15j"~"Organic C Bottle 15d",cycle=="Reacteur riche 45j"~"MEC org 100d",cycle=="Reacteur riche 45j OCV"~"OCV org 100d",cycle=="Reacteur pauvre 15j"~"MEC 125d",cycle=="Reacteur pauvre 15j OCV"~"OCV 125d",cycle=="Reacteur pauvre 100j"~"MEC 210d",cycle=="Reacteur pauvre 100j OCV"~"OCV 210d",cycle=="Reacteur pauvre 120j"~"MEC 230d",cycle=="Reacteur pauvre 120j OCV"~"OCV 230d",cycle=="Reacteur pauvre 200j"~"MEC 300d",cycle=="Reacteur pauvre 200j OCV"~"OCV 300d")),cycle=factor(cycle,levels=c("Organic C Bottle 00d","H2 Bottle 00d","H2 Bottle 45d","Organic C Bottle 15d","H2 Bottle 200d","H2 Bottle 300d","MEC org 100d","OCV org 100d","MEC 125d","OCV 125d","MEC 210d","OCV 210d","MEC 230d","OCV 230d","MEC 300d","OCV 300d"))),type=case_when(str_detect(cycle,"H2")~"H2",!str_detect(cycle,"H2")~str_sub(cycle,1,3))),enrich=case_when(type%in%c("H2","MEC","OCV")~type,type=="Org"~"MEC")),X16s=case_when(str_detect(cycle,"org 100d")~NaN,!str_detect(cycle,"org 100d")~X16s),bact=case_when(str_detect(cycle,"org 100d")~NaN,!str_detect(cycle,"org 100d")~bact))) |> filter(str_detect(cycle,"Org")) |> select(-enrich)) |> 
  filter(!(str_detect(cycle,"Bottle 00d")&str_detect(Sample,"Mix")))


plotcroiss<-ggplot(filter(summarize(group_by(qPCRfin,cycle,temps_abs),enrich=enrich,sebact=sd(bact),X16s=mean(X16s),nifH=mean(nifH),bact=mean(bact)),!is.na(X16s)))+
  geom_point(size=5,aes(x=temps_abs,y=bact,shape=enrich,color=enrich),position=position_dodge(width=1))+
  geom_path(linewidth=1.5,aes(x=temps_abs,y=bact,color=enrich))+
  geom_errorbar(aes(x=temps_abs,y=bact,ymin=bact-sebact,ymax=bact+sebact),position="dodge",linewidth=1)+
  labs(shape="",color="")+
  ylab("Calculated\nbacteria/mL")+
  scale_color_manual(values=c("green","blue","red","purple"))+
  xlab("time (days)")+
  theme_minimal(base_size = 30)+
  scale_y_log10()#+
#geom_text(aes(label=str_sub(nifH/X16s*100,1,4),x=as.numeric(str_extract(cycle,"[:digit:]{2,3}")),y=bact-0.2*bact),size=10,position=position_dodge(width=3))+
#geom_errorbar(aes(x=as.numeric(str_extract(cycle,"[:digit:]{2,3}")),y=bact,group=enrich),position=position_dodge(width=10))#+#scale_y_continuous(trans=log10_trans(),labels=trans_format("log10",function(x)(10^x)))+annotation_logticks(sides="l",scaled=F)
#coord_trans(y="log10")#+
scale_y_continuous(labels=trans_format("log10",math_format(10^.x)),breaks=c(10^6,1e7,1e8,1e9,1e10))
annotation_logticks(sides="l",scaled=T)



